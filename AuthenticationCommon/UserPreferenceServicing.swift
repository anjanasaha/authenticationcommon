//
//  UserPreferenceServicing.swift
//  Copyright © 2020 HashBytes App LLP. All rights reserved. 2020
//

import RxSwift

public protocol UserPreferenceServicing: class {
    
    var authenticationService: AuthenticationServicing { get }
    
    func allowMobileNotifications(_ allow: Bool) -> Observable<Void>
    
    func allowEmailNotifications(_ allow: Bool) -> Observable<Void>
    
    func allowSMSNotifications(_ allow: Bool) -> Observable<Void>
}
