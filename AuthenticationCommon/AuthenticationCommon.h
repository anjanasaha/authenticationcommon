//
//  AuthenticationCommon.h
//  Copyright © 2020 HashBytes App LLP. All rights reserved. 2020
//

#import <Foundation/Foundation.h>

//! Project version number for AuthenticationCommon.
FOUNDATION_EXPORT double AuthenticationCommonVersionNumber;

//! Project version string for AuthenticationCommon.
FOUNDATION_EXPORT const unsigned char AuthenticationCommonVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <AuthenticationCommon/PublicHeader.h>


