//
//  PermissionsServicing.swift
//  Copyright © 2020 HashBytes App LLP. All rights reserved. 2020
//

import RxSwift

public protocol PermissionObserving: AnyObject {
    var cameraPermissions: Observable<PermissionStatus> { get }
    var photoLibraryPermissions: Observable<PermissionStatus> { get }
}

public protocol PermissionManaging: PermissionObserving {
    func requestAccessCamera()
    func requestAccessPhotoLibrary()
}

public struct MediaPermissionStatus {
    var cameraPermissionStatus: MediaPermission
    var photoLibraryPermissionStatus: MediaPermission
    
    public init(cameraPermissionStatus: MediaPermission, photoLibraryPermissionStatus: MediaPermission) {
        self.cameraPermissionStatus = cameraPermissionStatus
        self.photoLibraryPermissionStatus = photoLibraryPermissionStatus
    }
}

public enum MediaPermission {
    case camera(PermissionStatus)
    case photoLibrary(PermissionStatus)
}

public enum PermissionStatus {
    case allowed
    case denied
    case unknown
    case restricted
}
