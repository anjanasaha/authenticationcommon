//
//  AuthenticationCommon.swift
//  Copyright © 2020 HashBytes App LLP. All rights reserved. 2020
//

import Foundation
import RxSwift

public protocol AuthenticationObserving: class {
    var currentUser: UserEntity? { get }
    
    func checkLogin()
}

public protocol AuthenticationManaging: AuthenticationObserving {
    
    /// Sign out
    func signOut() -> Observable<AuthenticationStatus>
    
    func signIn(with code: String) -> Observable<AuthenticationStatus>
    
    func link(email: String, password: String) -> Observable<AuthenticationStatus>
    
    /// Verify Phone Number
    func verify(phoneNumber: String) -> Observable<String>
    
    /// Verify code
    func verify(code: String) -> Observable<AuthenticationStatus>

}

public protocol AuthenticationServicing: AuthenticationManaging {

    /// Authentication status
    var authenticationStatus: Observable<AuthenticationStatus> { get }
}

public enum AuthenticationStatus {
    case loggedIn(Result<UserEntity,Error>)
    case loggedOut
}

public protocol UserEntity: AnyObject {
    
    var email: String? { get }
    
    var displayName: String? { get }
    
    var isNew: Bool { get }
    
    var hasEmail: Bool { get }

    var hasDisplayName: Bool { get }
}

enum AuthenticationServiceFailureReason: Int, Error {
    case unAuthorized = 401
    case userLoggedOut = 404
    case profileUpdateFailed = 1025
}

public enum AuthenticationServiceSource {
    case firebase
}
