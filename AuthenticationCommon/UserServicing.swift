//
//  UserPreferencesServicing.swift
//  Copyright © 2020 HashBytes App LLP. All rights reserved. 2020
//

import Foundation
import RxSwift
import UIKit

public enum ImageCompression {
    case compress(value: Float)
    case none
}

public enum UserProfileSize {
    case large
    case thumbnail
    case regular
}

public enum UserPreferences {
    case communication(UserPreferenceCommunication)
}

public enum UserProfile {
    case profilePhoto(UserProfilePhotoAction)
    case name(UserProfileNameAction)
    case userName(UserProfileUserameAction)
}

public enum UserProfilePhotoAction {
    case delete
    case update(UIImage, compression: ImageCompression)
}

public enum UserProfileNameAction {
    case update(String)
}

public enum UserProfileUserameAction {
    case update(String)
}

public enum UserPreferenceCommunication {
    case email(Bool)
    case sms(Bool)
    case notification(Bool)
}

public protocol UserServicing: class {
    
    func fetchProfilePhoto(size: UserProfileSize) -> Observable<Result<UIImage?,Error>>
    
    func update(preference: UserPreferences) -> Observable<Void>
    
    func update(profile: UserProfile) -> Observable<Void>
    
}

public class UserService: UserServicing {

    private var profileService: UserProfileServicing
    
    private var preferencesService: UserPreferenceServicing
    
    public func update(preference: UserPreferences) -> Observable<Void> {
        switch preference {
            case .communication(let communication):
                switch communication {
                case .notification(let allow):
                    return preferencesService.allowMobileNotifications(allow)
                case .email(let allow):
                    return preferencesService.allowEmailNotifications(allow)
                case .sms(let allow):
                    return preferencesService.allowSMSNotifications(allow)
            }
        }
    }
    
    public func update(profile: UserProfile) -> Observable<Void> {
        
        switch profile {
            case .name(let action):
                return profileService.process(action: action)
            case .profilePhoto(let action):
                return profileService.process(action: action)
            case .userName(let action):
                return profileService.process(action: action)
        }
    }
    
    public func fetchProfilePhoto(size: UserProfileSize) -> Observable<Result<UIImage?,Error>> {
        return profileService.fetchProfilePhoto()
    }
    
    public init(profileService: UserProfileServicing, preferencesService: UserPreferenceServicing) {
        self.preferencesService = preferencesService
        self.profileService = profileService
    }
}
