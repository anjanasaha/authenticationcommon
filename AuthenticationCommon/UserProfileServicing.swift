//
//  UserProfileServicing.swift
//  Copyright © 2020 HashBytes App LLP. All rights reserved. 2020
//

import RxSwift
import Foundation

public protocol UserProfileServicing: class {
    
    var authenticationService: AuthenticationServicing { get }
    
    func process(action: UserProfileNameAction) -> Observable<Void>
    
    func process(action: UserProfilePhotoAction) -> Observable<Void>
    
    func process(action: UserProfileUserameAction) -> Observable<Void>
    
    func fetchProfilePhoto() -> Observable<Result<UIImage?,Error>>
}
